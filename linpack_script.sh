#!/bin/bash
cd ~ &&
mkdir linpack;
cd linpack &&
wget www.netlib.org/benchmark/linpackc.new &&
mv linpackc.new linpack.c &&
gcc -O0 linpack.c -o linpack &&
for i in 1 2 3 4 5
do
	echo -e '5000\nq' | ./linpack > output_linpack$i.txt
done
mv output_* ~
cd ~
