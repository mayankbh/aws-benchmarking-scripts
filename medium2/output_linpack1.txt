Enter array size (q to quit) [200]:  Memory required:  195371K.


LINPACK benchmark, Double precision.
Machine precision:  15 digits.
Array size 5000 X 5000.
Average rolled and unrolled performance:

    Reps Time(s) DGEFA   DGESL  OVERHEAD    KFLOPS
----------------------------------------------------
       1  32.00  99.36%   0.12%   0.52%  655341.280

Enter array size (q to quit) [200]:  