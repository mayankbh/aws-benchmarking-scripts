Enter array size (q to quit) [200]:  Memory required:  195371K.


LINPACK benchmark, Double precision.
Machine precision:  15 digits.
Array size 5000 X 5000.
Average rolled and unrolled performance:

    Reps Time(s) DGEFA   DGESL  OVERHEAD    KFLOPS
----------------------------------------------------
       1  31.97  99.36%   0.12%   0.52%  655825.729

Enter array size (q to quit) [200]:  