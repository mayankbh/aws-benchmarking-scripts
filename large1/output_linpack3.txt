Enter array size (q to quit) [200]:  Memory required:  195371K.


LINPACK benchmark, Double precision.
Machine precision:  15 digits.
Array size 5000 X 5000.
Average rolled and unrolled performance:

    Reps Time(s) DGEFA   DGESL  OVERHEAD    KFLOPS
----------------------------------------------------
       1  32.22  99.34%   0.12%   0.54%  650891.653

Enter array size (q to quit) [200]:  