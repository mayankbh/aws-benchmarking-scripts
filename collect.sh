#!/bin/bash

#$1 = PEM
#$2 = IP
#$3 = folder to create

mkdir $3 &&
cd $3 &&
scp -i ../$1 ubuntu@$2:~/*.txt . &&
scp -i ../$1 ubuntu@$2:~/*.xls . &&
scp -i ../$1 ubuntu@$2:~/logfile . &&
echo "Done!"
