#!/bin/bash

sudo apt-get --assume-yes update &&
sudo apt-get --assume-yes install gcc &&
sudo apt-get --assume-yes install make &&

cd ~ &&
echo "Starting run now" > logfile &&
date >> logfile ;

cd ~ &&
echo "Starting LINPACK" >> logfile &&
nohup ./linpack_script.sh > /dev/null &&
cd ~ &&
echo "Finished LINPACK" >> logfile ;

cd ~ &&
echo "Starting iozone" >> logfile &&
nohup ./iozone_script.sh > /dev/null &&
cd ~ &&
echo "Finished iozone" >> logfile ;

cd ~ &&
echo "Starting Linux build" >> logfile &&
nohup ./linux_compile_script.sh > /dev/null &&
cd ~ &&
echo "Finished linux compilation" >> logfile ;

cd ~ &&
echo "Finished run" >> logfile &&
date >> logfile ;
