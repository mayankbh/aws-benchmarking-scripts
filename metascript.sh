#!/usr/bin/bash

#$1 = PEM file
#$2 = Public DNS
#$3 = Log file

scp -o StrictHostKeyChecking=no -i $1 linpack_script.sh iozone_script.sh linux_compile_script.sh run_nohup.sh ubuntu@$2:~ &&
echo "Copied files over" &&
ssh -o StrictHostKeyChecking=no -i $1 ubuntu@$2 'nohup bash run_nohup.sh' & 
