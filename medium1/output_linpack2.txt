Enter array size (q to quit) [200]:  Memory required:  195371K.


LINPACK benchmark, Double precision.
Machine precision:  15 digits.
Array size 5000 X 5000.
Average rolled and unrolled performance:

    Reps Time(s) DGEFA   DGESL  OVERHEAD    KFLOPS
----------------------------------------------------
       1  32.47  99.37%   0.12%   0.51%  645736.547

Enter array size (q to quit) [200]:  