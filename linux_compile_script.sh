#!/bin/bash

cd ~ &&
mkdir linux;
cd linux &&
wget https://www.kernel.org/pub/linux/kernel/v3.x/linux-3.9.3.tar.xz &&
for i in 1 2 3 4 5
do
	cd ~/linux &&
	rm -rf linux-3.9.3 ;
	{ time tar -xavf linux-3.9.3.tar.xz ; } 2> ~/untar_$i.txt &&
	cd linux-3.9.3 &&
	make distclean &&
	make defconfig &&
	{ time make all ; } 2> ~/linux_$i.txt
done
cd ~
